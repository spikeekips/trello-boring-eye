/* from https://markv.nl/blag/rainbow-colormap-in-javascript */
function hslToRgb(h, s, l) {
    var r, g, b;

    if (s == 0) {
        r = g = b = l; // achromatic
    } else {
        function hue2rgb(p, q, t) {
            if (t < 0) t += 1;
            if (t > 1) t -= 1;
            if (t < 1 / 6) return p + (q - p) * 6 * t;
            if (t < 1 / 2) return q;
            if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
            return p;
        }

        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        var p = 2 * l - q;
        r = hue2rgb(p, q, h + 1 / 3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1 / 3);
    }

    return [r * 255, g * 255, b * 255];
}

function cmap_rainbow(valcnt, cyclelen, s, l, stepsize) {
    if (cyclelen == null) cyclelen = valcnt;
    var cm = new Array();
    /* Deliberately exclude the endpoint because it's cyclic. */
	if (cyclelen <= 2) {
		var step = 360;
  } else {
    	var step = 360. / (cyclelen - 1);
  }
  for (var i = 0; i < valcnt * stepsize; i += stepsize) {
      let h = Math.round((i % cyclelen) * step);
      let rgb = hslToRgb(h / 360., s / 100., l / 100.);
      cm.push('rgb(' + Math.round(rgb[0]) + ',' + Math.round(rgb[1]) + ',' + Math.round(rgb[2]) + ')');
  }
  return cm;
}
/* -- */

var Board = React.createClass({
  getDefaultProps: function() {
    return {
      config: {},
      board: []
    }
  },
  getInitialState: function() {
    return {
      list: {},
      cards: []
    }
  },
  componentWillMount: function() {
    if (! this.props.board.list) {
      return;
    }
    if (! this.props.board.data) {
      return;
    }

    window.Trello.get('/boards/' + this.props.board.data.id + '/lists', this.onBoardLists);
  },
  componentDidMount: function() {
    this.scrollList();
  },

  onBoardLists: function(data) {
    var found = data.filter(function(l) {
      if (l.name.toLowerCase() != this.props.board.list.toLowerCase()) {
        return false;
      }
      return true;
    }.bind(this));

    if (found.length < 1) {
      console.error(this.props.board.name, ':', this.props.board.list, 'not found');
      return;
    }

    this.setState({list: found[0]});

    this.updateCards();
  },
  updateCards: function() {
    //console.log('update cards:', this.props.board.name + ',', this.state.list.name);

    window.Trello.get(
      '/list/' + this.state.list.id + '/cards?members=true&member_fields=fullName,initials,avatarHash',
      this.onBoardCards
    );
  },
  onBoardCards: function(data) {
    var sorted = data.sort(function(a, b) {
      if (! a.due && ! b.due) {
        return true;
      }
      if (! a.due) {
        return true;
      }
      if (! b.due) {
        return false;
      }

      return moment(a.due).diff(moment(b.due)) > 0;
    });

    this.setState({cards: sorted});

    setTimeout(this.updateCards, 1000 * 60);
  },

  scrollList: function() {
      setTimeout(this.scrollListInner, 2000);
  },
  scrollListInner: function() {
    if (typeof this.currentPos == 'undefined') {
      this.currentPos = 1;
    }

    if (this.currentPos > this.state.cards.length) {
      this.currentPos = 1;
    }

    if (this.state.cards.length < 5) {
      this.scrollList();
      return;
    }

    var scroll = $('#board-' + this.props.board.data.id + ' .list-cards');

    if (scroll.height() + 30 > scroll.parent().height()) {
      this.scrollList();
      return;
    }

    var target = $('#board-' + this.props.board.data.id + ' .list-cards .list-card:nth-child(' + this.currentPos + ')');
    var lastScrollTop = scroll.scrollTop();

    $.smoothScroll({
      speed: 200,
      scrollElement: scroll,
      scrollTarget: target,
      afterScroll: function() {
        this.currentPos += 1;

        if (this.currentPos > 2 && lastScrollTop == scroll.scrollTop()) {
          this.currentPos = 1;
        }

        this.scrollList();
      }.bind(this)
    });
  },

  render: function() {
    return (
			<div className='js-list list-wrapper' id={ 'board-' + this.props.board.data.id }>
				<div className='list js-list-content' style={{ backgroundColor: this.props.board.color }}>
					<div className='list-header js-list-header u-clearfix is-menu-shown'>
						<div className='list-header-target js-editing-target'></div>
						<div aria-label={ this.props.board.name } className='list-header-name mod-list-name js-list-name-input'
              dir='auto' maxLength='512' spellCheck='false'
              style={{ overflow: 'hidden', wordWrap: 'break-word'}}
              ><span className='count'>{ this.state.cards.length }</span> { this.props.board.name + ': ' + this.state.list.name }</div>
					</div>
					<div className='list-cards u-fancy-scrollbar u-clearfix js-list-cards js-sortable ui-sortable'>

      {
          this.state.cards.map(function(d, idx) {
            return <Card data={ d } key={ idx } />
          })
      }
					</div>
				</div>
			</div>
    )
  }

});


var Card = React.createClass({
  render: function() {
    var due_date = moment(this.props.data.due);
    var now = moment(new Date());

    var expired = false;
    var expired_hours = due_date.diff(now, 'hours')
    if (expired_hours <= 0) {
      expired = true;
    } else if (due_date.format('YYYYMMDD') == now.format('YYYYMMDD')) {
      expired = true;
    }

    var due_humanize = '';
    if (due_date.isValid()) {
      due_humanize = due_date.fromNow(false);
    }

    return (
			<a className='list-card js-member-droppable ui-droppable' target={ this.props.data.id } href={ this.props.data.url } id={ this.props.data.id }>
        <div className='list-card-cover js-card-cover'></div>
        <span className='icon-sm icon-edit list-card-operation dark-hover js-open-quick-card-editor js-card-menu'></span>
        <div className='list-card-stickers-area hide'>
          <div className='stickers js-card-stickers'></div>
        </div>
        <div className='list-card-details'>
          <span className='list-card-title js-card-name' dir='auto'>
            { this.props.data.name }
          </span>
      { ! due_date.isValid() ? '' : 
          <div className='badges'>
            <span className='js-badges'></span>
            <div className={'badge ' + (expired ? 'is-due-now' : '')} title='This card is recently overdue!'>
              <span className='badge-icon icon-sm icon-clock'></span>
              <span className='badge-text'>{ due_humanize }</span>
            </div>
          </div>
      }
          <div className='list-card-members js-list-card-members'>

      {
        this.props.data.members.map(function(m, idx) {
            return (
              <div key={ idx } className='member js-member-on-card-menu' data-idmem={ m.id }>
              {m.avatarHash ? 
                <img className='member-avatar' height='30'
                  src={ 'https://trello-avatars.s3.amazonaws.com/' + m.avatarHash + '/30.png' }
                  width='30' />
                :
                <span className="member-initials" title={ m.fullName }>{ m.initials }</span>
              }
              </div>
            )
        })
      }
          </div>
        </div>
      </a>
    )
  }
});

var App = React.createClass({
  getDefaultProps: function() {
    return {
      config: {}
    }
  },
  getInitialState: function() {
    return {
      boards: [] // board data
    }
  },
  componentWillMount: function() {
    setTimeout(function() {
      window.Trello.get('/organizations/' + this.props.config.org_id + '/boards', this.onBoards, this.errBoards);
    }.bind(this), 100);
  },
  onBoards: function(data) {
    var boards = new Array();

    var boardNames = this.props.config.boards.map(function(b) {
      return b.name.toLowerCase();
    });
    var filtered = {};
    data.map(function(d) {
      if (! boardNames.includes(d.name.toLowerCase())) {
        return;
      }
      filtered[d.name.toLowerCase()] = d;
    });

    var cm = cmap_rainbow(this.props.config.boards.length, 107, 100, 10, 15);
    var boards = [];
    this.props.config.boards.map(function(b, idx) {
      if (! b.list) {
        console.error(b.name, ': list is missing');
        return;
      }

      var o = $.extend({}, b);
      o['data'] = filtered[o.name.toLowerCase()];
      o['color'] = cm[idx];

      boards.push(o);
    });

    this.setState({boards: boards});
  },
  errBoards: function() {
    console.error(arguments);
  },
  render: function() {
    return (
      <div className='board-main-content'>
        <div className='board-header u-clearfix js-board-header'>
          <a className='board-header-btn board-header-btn-name no-edit board-header-btn-name-with-org-logo' href='#'>
            <img className='board-header-btn-name-org-logo'
              src='https://trello-logos.s3.amazonaws.com/a78170664b05630a5523047e3176e206/30.png'
              srcSet='https://trello-logos.s3.amazonaws.com/a78170664b05630a5523047e3176e206/30.png 1x, https://trello-logos.s3.amazonaws.com/a78170664b05630a5523047e3176e206/170.png 2x' />
            <span className='board-header-btn-text' dir='auto'>Boring <span className='eye'>👁</span>y<span className='eye'>👁</span>: BlockchainOS</span>
          </a>
        </div>
        <div className='board-warnings u-clearfix js-board-warnings hide'></div>
        <div className='board-canvas'>

          <div className='u-fancy-scrollbar js-no-higher-edits js-list-sortable ui-sortable' id='board'>
      {
          this.state.boards.map(function(b, idx) {
            if (! b.data) return;
            return <Board key={ idx } board={ b } config={ this.props.config } />
          }.bind(this))
      }

          </div>

        </div>
      </div>
    )
  }
});


var authenticationSuccess = function() {
  console.log('Successful authentication');

  ReactDOM.render(
    <App config={ CONFIG } />,
    document.getElementById('app')
  );
};

var authenticationFailure = function() {
  console.log('Failed authentication');

  authorize();
};

var authorize = function() {
  window.Trello.authorize({
    type: 'popup',
    name: 'BlockchainOS Trello Swallower',
    scope: {
      read: 'true',
      write: 'true'
    },
    expiration: 'never',
    success: authenticationSuccess,
    error: authenticationFailure
  });
}

var run = function() {
  $.ajax({
    url: 'config.json',
    dataType: 'json',
    error: function() {
      console.error('failed to load config:', arguments);
      window.alert(arguments[2]);
    },
    success: function(data) {
      console.log('config loaded:', data);
      CONFIG = data;

      authorize();
    }
  });
}

var CONFIG = null;

$(document).ready(run);
